import unittest
from pyspark.sql import SparkSession
import datetime

from main import process_events

class PySparkTestCase(unittest.TestCase):
    """Set-up of global test SparkSession"""

    @classmethod
    def setUpClass(cls):
        cls.spark = (SparkSession.builder.master("local[1]").appName("PySpark integration test").getOrCreate())

    @classmethod
    def tearDownClass(cls):
        cls.spark.stop()


class TestReadConcatenateJson(PySparkTestCase):
    def test_read_concatenate_json(self):

        process_events(self.spark, './io/input/test/','./io/output/integration')

        df_expected = self.spark.createDataFrame(
            data=[
                ["b24967a6-3592-44f3-96ff-6a85d2cd5777","account","creation",'{dummy=dummy}', datetime.datetime(2021, 1, 24, 17, 41, 3)],
                ["0a384c35-7690-4fff-b5f7-d710eafa285c","account","creation",'{dummy=dummy}', datetime.datetime(2021, 2, 3, 10, 26, 5)]],
            schema=['event_id', 'domain', 'event_type', 'data', 'datetime'])


        df = self.spark.read.parquet("io/output/integration/account_creation/year=2021/month={1,2}/*")
        df.show(20, False)

        self.assertEqual(set(df.collect()), set(df_expected.collect()))


if __name__ == '__main__':
    unittest.main()