import unittest
from pyspark.sql import SparkSession
import datetime

from main import read_concatenate_json

class PySparkTestCase(unittest.TestCase):
    """Set-up of global test SparkSession"""

    @classmethod
    def setUpClass(cls):
        cls.spark = (SparkSession
                     .builder
                     .master("local[1]")
                     .appName("PySpark unit test")
                     .getOrCreate())

    @classmethod
    def tearDownClass(cls):
        cls.spark.stop()


class TestReadConcatenateJson(PySparkTestCase):
    def test_read_concatenate_json(self):
        df = read_concatenate_json(self.spark, './io/input/test/')

        df_expected = self.spark.createDataFrame(
            data=[["8bcb0d71-a0af-42ec-9338-ac2327b3c901","2021-01-24T17:40:16","transaction","creation",'{dummy=dummy}'],
                ["feb11eee-b0a7-4079-9e28-f2ba0c1b44f1","2021-01-24T17:40:21","transaction","creation",'{dummy=dummy}'],
                ["b24967a6-3592-44f3-96ff-6a85d2cd5777","2021-01-24T17:40:54","account","creation",'{dummy=dummy}'],
                ["b24967a6-3592-44f3-96ff-6a85d2cd5777","2021-01-24 17:41:03","account","creation",'{dummy=dummy}'],
                ["1cbf065e-8920-45ce-9e90-5680bd0c66f0","2021-01-24T17:43:16","transaction","creation",'{dummy=dummy}'],
                ["e29e6e99-35d1-4c2f-a169-ec7b1bd11b55","2021-01-24T17:47:39","transaction","creation",'{dummy=dummy}'],
                ["0a384c35-7690-4fff-b5f7-d710eafa285c","2021-02-03T10:26:05","account","creation",'{dummy=dummy}'],
                ["4bfdd7d0-1fc6-411d-9c25-fc609d3b6a27","2021-02-03T10:28:41","transaction","creation",'{dummy=dummy}'],
                ["4bfdd7d0-1fc6-411d-9c25-fc609d3b6a27","2021-02-03 10:28:49","transaction","creation",'{dummy=dummy}']],
            schema=['event_id', 'domain', 'event_type', 'data', 'timestamp'])

        self.assertEqual(set(df.collect()), set(df_expected.collect()))


if __name__ == '__main__':
    unittest.main()