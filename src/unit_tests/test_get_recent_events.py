import unittest
from pyspark.sql import SparkSession
import datetime

from main import get_recent_events_dataframe

class PySparkTestCase(unittest.TestCase):
    """Set-up of global test SparkSession"""

    @classmethod
    def setUpClass(cls):
        cls.spark = (SparkSession
                     .builder
                     .master("local[1]")
                     .appName("PySpark unit test")
                     .getOrCreate())

    @classmethod
    def tearDownClass(cls):
        cls.spark.stop()


class TestReadConcatenateJson(PySparkTestCase):
    def test_get_recent_events_dataframe(self):
        df_events = self.spark.createDataFrame(
            data=[["8bcb0d71-a0af-42ec-9338-ac2327b3c901",datetime.datetime(2021, 1, 24, 17, 40, 16),"transaction","creation",'{dummy=dummy}'],
                ["feb11eee-b0a7-4079-9e28-f2ba0c1b44f1",datetime.datetime(2021, 1, 24, 17, 40, 21),"transaction","creation",'{dummy=dummy}'],
                ["b24967a6-3592-44f3-96ff-6a85d2cd5777",datetime.datetime(2021, 1, 24, 17, 40, 54),"account","creation",'{dummy=dummy}'],
                ["b24967a6-3592-44f3-96ff-6a85d2cd5777",datetime.datetime(2021, 1, 24, 17, 41, 3),"account","creation",'{dummy=dummy}'],
                ["1cbf065e-8920-45ce-9e90-5680bd0c66f0",datetime.datetime(2021, 1, 24, 17, 43, 16),"transaction","creation",'{dummy=dummy}'],
                ["e29e6e99-35d1-4c2f-a169-ec7b1bd11b55",datetime.datetime(2021, 1, 24, 17, 47, 39),"transaction","creation",'{dummy=dummy}'],
                ["0a384c35-7690-4fff-b5f7-d710eafa285c",datetime.datetime(2021, 2, 3, 10, 26, 5),"account","creation",'{dummy=dummy}'],
                ["4bfdd7d0-1fc6-411d-9c25-fc609d3b6a27",datetime.datetime(2021, 2, 3, 10, 28, 41),"transaction","creation",'{dummy=dummy}'],
                ["4bfdd7d0-1fc6-411d-9c25-fc609d3b6a27",datetime.datetime(2021, 2, 3, 10, 28, 49),"transaction","creation",'{dummy=dummy}']],
                schema=['event_id', 'datetime', 'domain', 'event_type', 'data'])

        df = get_recent_events_dataframe(df_events, 'event_id', 'datetime')

        df_expected = self.spark.createDataFrame(
            data=[["8bcb0d71-a0af-42ec-9338-ac2327b3c901",datetime.datetime(2021, 1, 24, 17, 40, 16),"transaction","creation",'{dummy=dummy}'],
                ["feb11eee-b0a7-4079-9e28-f2ba0c1b44f1",datetime.datetime(2021, 1, 24, 17, 40, 21),"transaction","creation",'{dummy=dummy}'],
                ["b24967a6-3592-44f3-96ff-6a85d2cd5777",datetime.datetime(2021, 1, 24, 17, 41, 3),"account","creation",'{dummy=dummy}'],
                ["1cbf065e-8920-45ce-9e90-5680bd0c66f0",datetime.datetime(2021, 1, 24, 17, 43, 16),"transaction","creation",'{dummy=dummy}'],
                ["e29e6e99-35d1-4c2f-a169-ec7b1bd11b55",datetime.datetime(2021, 1, 24, 17, 47, 39),"transaction","creation",'{dummy=dummy}'],
                ["0a384c35-7690-4fff-b5f7-d710eafa285c",datetime.datetime(2021, 2, 3, 10, 26, 5),"account","creation",'{dummy=dummy}'],
                ["4bfdd7d0-1fc6-411d-9c25-fc609d3b6a27",datetime.datetime(2021, 2, 3, 10, 28, 49),"transaction","creation",'{dummy=dummy}']],
                schema=['event_id', 'datetime', 'domain', 'event_type', 'data'])

        self.assertEqual(set(df.collect()), set(df_expected.collect()))

if __name__ == '__main__':
    unittest.main()