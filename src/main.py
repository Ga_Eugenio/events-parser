# import findspark
# findspark.init()

import json
import argparse

from pyspark.sql import SparkSession, Row, DataFrame
from pyspark.sql.types import StructType, StructField, StringType
from pyspark.sql.functions import col, to_timestamp, desc, dense_rank, count, year, month, dayofmonth
from pyspark.sql.window import Window


def read_concatenate_json(spark, input_path: str):

    schema = StructType([
        StructField("event_id",StringType(),True),
        StructField("timestamp",StringType(),True),
        StructField("domain",StringType(),True),
        StructField("event_type", StringType(), True),
        StructField("data", StringType(), True)
    ])

    df_concatenate_json = spark.read.text(input_path)
    df_events = df_concatenate_json.rdd.flatMap(lambda row: json.loads("[{}]".format(row.value.replace('}{', '},{')))) \
                    .toDF(schema)

    return df_events


def get_recent_events_dataframe(events: DataFrame, event_columns, datetime_column: str ):
    w = Window.partitionBy(event_columns).orderBy(desc(datetime_column))
    df_ordered_events = events.withColumn('order_registry',dense_rank().over(w))
    # df_ordered_events.show()
    df_ordered_events.count()

    df_recent_events = df_ordered_events.filter(df_ordered_events.order_registry == 1).drop(df_ordered_events.order_registry)
    # df_recent_events.show()
    df_recent_events.count()

    return df_recent_events

def write_parquet_events(events: DataFrame, partition_columns, output_path: str):

    events_type_list = events.select('domain', 'event_type').distinct().collect()

    for row in events_type_list:

        events.filter( (events.domain == row.domain) & (events.event_type == row.event_type)) \
            .write \
            .mode('overwrite') \
            .partitionBy(partition_columns) \
            .parquet('{}/{}_{}'.format(output_path,row.domain, row.event_type)) 


def process_events(spark, input_path, output_path):

    df_events = read_concatenate_json(spark, input_path)

    df_events = df_events \
                    .withColumn("datetime",to_timestamp("timestamp")) \
                    .drop("timestamp")

    df_recent_events = get_recent_events_dataframe(df_events, 'event_id', 'datetime')

    df_recent_events = df_recent_events.withColumn("year", year(col("datetime"))) \
                    .withColumn("month", month(col("datetime"))) \
                    .withColumn("day", dayofmonth(col("datetime")))

    write_parquet_events(df_recent_events, ['year', 'month', 'day'], output_path)


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_path", help="Input path that contains the events files")
    parser.add_argument("--output_path", help="Output path that will contain the parquet output files")
    args = parser.parse_args()

    spark = SparkSession.builder.appName("Events Parser").master("local[*]").getOrCreate()

    process_events(spark,args.input_path, args.output_path)

    spark.stop()
