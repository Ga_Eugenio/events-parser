# Events Parser⠀
## Contents
***
- Jupyter used for development - The result files will be written to **io/output/jupyter**
- IO Dir
    - Input Dir:
      - *original* - All original json files events 
      - *test_two_files* - Containt two files of the original dir
      - *test* - Contain the files used for unit and integration test
- SRC - Python solution and tests

⠀
## How to Run
***  

### **Solution**

Run the following command to process all the events files 

> `spark-submit --conf spark.ui.port=5051 src/main.py  --input_path io/input/original --output_path io/output/original`

Args:
- input_path: Events json path to process
- output_path: Path were the parquet files will be written

The result files will be written to **io/output/original**

***
### **Tests**

To run the unit tests execute the following command
> `python3 -m unittest discover -s unit_tests -t src`

⠀⠀

To run the integration test execute the following command
> `python3 -m unittest discover -s integration_test -t src`
 
The result files will be written to **io/output/integration_test**
